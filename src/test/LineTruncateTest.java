package test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import main.LineTruncate;

public class LineTruncateTest {
	String newline = System.getProperty("line.separator");
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

	@Before
	public void setUpStreams() {
	    System.setOut(new PrintStream(outContent));
	}

	@After
	public void cleanUpStreams() {
	    System.setOut(null);
	}
	@Test
	public void testExampleGiven() {
		
		String correctResult = "Tom exhibited."+
		newline+"Amy Lawrence was proud and glad, and she... <Read More>"+
		newline+"Tom was tugging at a button-hole and looking sheepish."+
		newline+"Two thousand verses is a great many - ve... <Read More>"+
		newline+"Tom's mouth watered for the apple, but h... <Read More>"+newline;
		LineTruncate.truncate("sampleInput");
		assertEquals(correctResult,outContent.toString());
	}
	@Test
	public void testEmptyFileName() {
		LineTruncate.truncate("");
		assertEquals("The filename is empty!"+newline,outContent.toString());
	}
	@Test
	public void testNonExistantFile() {
		LineTruncate.truncate("#%$$#^%#@.....,,.,.,");
		assertEquals("The file doesn't exist, please make sure the full path is included"+newline,outContent.toString());
	}
}
