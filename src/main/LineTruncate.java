package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class LineTruncate {

	public static void main(String[] args) {
		truncate(args[0]);
	}

	public static void truncate(String fileName) {
		if (fileName=="") {
			System.out.println("The filename is empty!");
			return;
		}
		fileName = addTxtFileExtension(fileName);
		fileName = addFullPath(fileName);
		if (new File(fileName).exists()) { 
			try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
				String line;
				while ((line = br.readLine()) != null) {
					// process the line.
					if (line.length()<=55) {
						System.out.println(line);
					} else {
						System.out.println(line.substring(0, 40)+"... <Read More>");
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println("The file doesn't exist, please make sure the full path is included");
		}
	}

	private static String addFullPath(String fileName) {
		if (!(fileName.substring(1, 2).equals(new String(":")))) {
			fileName=new File("").getAbsolutePath()+"\\"+fileName;
		}
		return fileName;
	}

	private static String addTxtFileExtension(String fileName) {
		if (!(fileName.substring(fileName.length()-4).equals(new String(".txt")))) {
			fileName=fileName+".txt";
		}
		return fileName;
	}
}
